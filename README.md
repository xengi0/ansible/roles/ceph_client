ceph_client role
================

Setup ceph and configure cephfs and rbd volumes.

Requirements
------------

This role assumes the following:

  - User called `ansible` is present on the system
  - User `ansible` can use sudo without password
  - User `ansible` uses `/bin/sh` compatible shell

Role Variables
--------------

Global variables that need to be present:

- `ceph_client__cephfs`: List of cephfs mountpoints to configure
- `ceph_client__rbd`: List of RBD volumes to configure **[Not yet implemented]**

Dependencies
------------

None

Example Playbook
----------------


```yaml
# inventory.yml
---
all:
  hosts:
    server1:
      ceph_client__cephfs:
        - path: "/srv/shared_volume"
          src: ":/"
          fs: "my_share"
    server2:
      ceph_client__rbd:
        - path: "/data"
          src: ""
```

```yaml
# playbook.yml
---
- hosts: all
  roles:
     - ceph_client
```

License
-------

MIT

Author Information
------------------

  - Ricardo (XenGi) Band <email@ricardo.band>

